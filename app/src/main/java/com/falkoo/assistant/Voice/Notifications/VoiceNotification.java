package com.falkoo.assistant.Voice.Notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class VoiceNotification {

    public void createNotification(Context context) {
        // Build notification
        Notification noti = new Notification.Builder(context)
                .setContentTitle("Voice")
                .setSmallIcon(android.R.drawable.arrow_up_float)
                .addAction(android.R.drawable.ic_delete, "STOP",
                        getPendingAction(context, 0))
                .addAction(android.R.drawable.ic_input_add, "START",
                        getPendingAction(context, 1))
                .build();

        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, noti);

    }

    private PendingIntent getPendingAction(Context context, int action) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(context, VoiceNotificationReceiver.class);
        intent.putExtra("ACTION", action);

        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
