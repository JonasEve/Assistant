package com.falkoo.assistant.Voice.Notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.falkoo.assistant.Voice.Services.VoiceTask;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class VoiceNotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle answerBundle = intent.getExtras();
        int userAnswer = answerBundle.getInt("ACTION");
        if(userAnswer == 0)
        {
           VoiceTask.getInstance().stopListening();
        }
        else if(userAnswer == 1)
        {
            VoiceTask.getInstance().startListening();
        }
    }
}
