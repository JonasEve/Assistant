package com.falkoo.assistant.Voice.Services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.widget.Toast;

import com.falkoo.assistant.Strategies.ActionStrategies.ActivateDesactivateActionStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.CallStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.ChangeAIName;
import com.falkoo.assistant.Strategies.ActionStrategies.ChangeMyNameStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.CommandStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.LaunchAppStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.MailStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.MessageStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.MuteStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.ReadMessageStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.TalkStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.WebSearchStrategy;
import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;
import com.falkoo.assistant.Views.SpeakerListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Pattern;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class VoiceTask implements
        RecognitionListener, android.speech.RecognitionListener {

    private final SharedPreferences pref;
    private final String KEYWORD = "mange";
    private Intent recognizerIntent;
    private edu.cmu.pocketsphinx.SpeechRecognizer speech;
    private android.speech.SpeechRecognizer speechGoogle;

    private TextToSpeech textToSpeech;
    private IActionStrategy strategy;
    private int state = -1;
    private AudioManager mAudioManager;
    private String name;
    private String me;
    private boolean mute;
    private SpeakerListener listener;

    public Context context;
    private static VoiceTask instance;

    public static VoiceTask newInstance(Context context) {

        if (instance == null)
            instance = new VoiceTask(context);

        return instance;
    }

    public static VoiceTask getInstance() {

        if (instance == null)
            throw new AssertionError("Context null");

        return instance;
    }

    private VoiceTask(final Context context) {
        this.context = context;

        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.FRANCE);
                }
            }
        });

        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 50, 0);
            }

            @Override
            public void onDone(String utteranceId) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                Handler mainHandler = new Handler(context.getMainLooper());
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        if (listener != null)
                            listener.onSpeackStatusChanged(true);
                        speechGoogle.startListening(recognizerIntent);
                    }
                };
                mainHandler.post(myRunnable);
            }

            @Override
            public void onError(String utteranceId) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            }
        });

        pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        refreshSettings();

        Assets assets;
        File assetDir;
        try {
            assets = new Assets(context);
            assetDir = assets.syncAssets();

            speech = SpeechRecognizerSetup.defaultSetup()
                    .setAcousticModel(new File(assetDir, "fr-fr-ptm"))
                    .setDictionary(new File(assetDir, "cmudict-fr-fr.dict"))
                    .setKeywordThreshold(1e-45f)
                    .setBoolean("-allphone_ci", true)
                    .getRecognizer();
        } catch (IOException e) {
            e.printStackTrace();
        }

        speech.addListener(this);
        speech.addKeyphraseSearch(KEYWORD, KEYWORD);

        recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fr-FR");
        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);

        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    public void setVoiceStackState(int step, IActionStrategy strategy) {
        this.state = step;
        this.strategy = strategy;
    }

    public void setSpeakerListener(SpeakerListener listener) {
        this.listener = listener;
    }

    public void startListening() {
        refreshSettings();

        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        speech.startListening(KEYWORD);
    }

    private void refreshSettings() {
        name = pref.getString("name", "Jarvis");
        me = pref.getString("me", "");
        mute = pref.getBoolean("mute", false);
    }

    public void stopListening() {
        state = -1;
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 50, 0);
        if (speechGoogle != null)
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (listener != null)
                        listener.onSpeackStatusChanged(false);
                    speechGoogle.cancel();
                }
            });
        speech.cancel();
    }

    public void process(final String match) {
        process(new SplitableArrayList(new ArrayList<String>() {{
            add(match);
        }}));
    }

    private void process(SplitableArrayList matches) {

        if (matches != null && state > -1) {

            refreshSettings();

            if (contains(matches, "stop") != null) {
                state = -1;
                speechGoogle.cancel();
                feedBack(LanguageUtils.getInstance().getLanguage().farewell() + " " + me);
                return;
            }

            if (contains(matches, "annuler") != null) {
                state = -1;
                feedBack(LanguageUtils.getInstance().getLanguage().bye() + " " + me);
                return;
            }

            switch (state) {

                case 0:
                    if (DetectAction(matches)) break;

                    String missingInfos = strategy.extractInformations(matches, context);
                    if (missingInfos != null) {
                        state = 1;
                        feedBack(missingInfos);
                    } else {
                        state = -1;
                        feedBack(strategy.execute(context));
                    }
                    break;

                case 1:
                    String valid = strategy.extractInformations(matches, context);

                    if (valid != null) {
                        feedBack(valid);
                        return;
                    } else {
                        state = -1;
                        feedBack(strategy.execute(context) + " " + me);
                    }
                    break;
                case 2:
                    if (contains(matches, strategy.getKeyWords()) != null) {
                        state = -1;
                        feedBack(strategy.execute(context));
                    }
                    break;
            }

        } else if (matches != null && contains(matches, name) != null) {
            state = 0;
            SplitableArrayList newMatches = matches.getSplitArrayList(name);
            if (newMatches.size() > 0)
                process(newMatches);
            else
                feedBack(LanguageUtils.getInstance().getLanguage().listen() + " " + me);
        } else {
            restartListening();
        }
    }

    public void feedBack(String value) {
        value = value == null ? "" : value;
        if (mute) {
            Toast.makeText(context, value, Toast.LENGTH_SHORT).show();
            restartListening();
        } else
            textToSpeech.speak(value, TextToSpeech.QUEUE_FLUSH, null, value);
    }

    private void restartListening() {
        //if(state == -1) {
        if (listener != null)
            listener.onSpeackStatusChanged(false);
        speech.startListening(KEYWORD);
        //}
        /*else if(speechGoogle != null)
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(listener != null)
                        listener.onSpeackStatusChanged(true);
                    speechGoogle.startListening(recognizerIntent);
                }
            });*/
    }

    private boolean DetectAction(ArrayList<String> matches) {
        String keyWord;
        if ((keyWord = contains(matches, CallStrategy.KEYWORDS)) != null) {
            strategy = new CallStrategy(keyWord);
        } else if ((keyWord = contains(matches, MessageStrategy.KEYWORDS)) != null) {
            strategy = new MessageStrategy(keyWord);
        } else if ((keyWord = contains(matches, MailStrategy.KEYWORDS)) != null) {
            strategy = new MailStrategy(keyWord);
        } else if ((keyWord = contains(matches, CommandStrategy.KEYWORDS)) != null) {
            strategy = new CommandStrategy(keyWord);
        } else if ((keyWord = contains(matches, LaunchAppStrategy.KEYWORDS)) != null) {
            strategy = new LaunchAppStrategy(keyWord);
        } else if ((keyWord = contains(matches, ChangeMyNameStrategy.KEYWORDS)) != null) {
            strategy = new ChangeMyNameStrategy(keyWord);
        } else if ((keyWord = contains(matches, ChangeAIName.KEYWORDS)) != null) {
            strategy = new ChangeAIName(keyWord);
        } else if (contains(matches, MuteStrategy.KEYWORDS) != null) {
            strategy = new MuteStrategy();
        } else if (contains(matches, TalkStrategy.KEYWORDS) != null) {
            strategy = new TalkStrategy();
        } else if ((keyWord = contains(matches, WebSearchStrategy.KEYWORDS)) != null) {
            strategy = new WebSearchStrategy(keyWord);
        } else if (contains(matches, ReadMessageStrategy.KEYWORDS) != null) {
            strategy = new ReadMessageStrategy();
        } else if ((keyWord = contains(matches, ActivateDesactivateActionStrategy.KEYWORDS)) != null) {
            strategy = new ActivateDesactivateActionStrategy(keyWord);
        } else {
            feedBack(LanguageUtils.getInstance().getLanguage().dontUnderstand() + " " + me);
            return true;
        }
        return false;
    }

    private String contains(ArrayList<String> matches, ArrayList<String> keywords) {
        for (String match : matches) {
            for (String keyword : keywords) {
                Pattern regex = Pattern.compile(keyword);
                if (match.contains(keyword) || regex.matcher(match).matches())
                    return keyword;
            }
        }

        return null;
    }

    private String contains(ArrayList<String> matches, final String keyword) {
        return contains(matches, new ArrayList<String>() {{
            add(keyword);
        }});
    }

    public void initSpeechGoogle() {
        if (speechGoogle != null)
            speechGoogle.cancel();

        speechGoogle = SpeechRecognizer.createSpeechRecognizer(context);
        speechGoogle.setRecognitionListener(VoiceTask.this);
    }

    //region SpeechGoogle
    @Override
    public void onReadyForSpeech(Bundle params) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float rmsdB) {

    }

    @Override
    public void onBufferReceived(byte[] buffer) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int error) {
        speechGoogle.cancel();
        restartListening();
    }

    @Override
    public void onResults(Bundle results) {

        ArrayList<String> matches = results
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        process(new SplitableArrayList(matches));
    }

    @Override
    public void onPartialResults(Bundle partialResults) {

        ArrayList<String> matches = partialResults
                .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

        process(new SplitableArrayList(matches));
    }

    @Override
    public void onEvent(int eventType, Bundle params) {

    }
    //endregion

    //region Speech
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        final String text = hypothesis.getHypstr();
        if (text.contains(KEYWORD)) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    initSpeechGoogle();
                    if (listener != null)
                        listener.onSpeackStatusChanged(true);
                    speechGoogle.startListening(recognizerIntent);
                }
            });
            speech.cancel();
        }
    }

    @Override
    public void onResult(Hypothesis hypothesis) {

    }

    @Override
    public void onError(Exception e) {

    }

    @Override
    public void onTimeout() {

    }
    //endregion
}
