package com.falkoo.assistant.Views;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.falkoo.assistant.R;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class SettingsActivity extends AppCompatActivity {

    private String command;
    private String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText input = new EditText(SettingsActivity.this);

                input.setInputType(InputType.TYPE_CLASS_TEXT);

                final EditText input2 = new EditText(SettingsActivity.this);

                input2.setInputType(InputType.TYPE_CLASS_TEXT);

                new AlertDialog.Builder(SettingsActivity.this).setMessage("Saisir la commande vocale").setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        command = input.getText().toString();
                        new AlertDialog.Builder(SettingsActivity.this).setMessage("Saisir la requête").setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                query = input.getText().toString();
                                SharedPreferences preferences = SettingsActivity.this.getSharedPreferences("command", MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(command, query);
                                editor.apply();
                            }
                        }).setView(input2).show();
                    }
                }).setView(input).show();
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText input = new EditText(SettingsActivity.this);

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                final SharedPreferences pref = getSharedPreferences("command", Context.MODE_PRIVATE);
                input.setText(pref.getString("endpoint", ""));

                new AlertDialog.Builder(SettingsActivity.this).setMessage("Saisir le host").setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("endpoint", input.getText().toString());
                        editor.apply();
                    }
                }).setView(input).show();
            }
        });

        final SharedPreferences pref = getSharedPreferences("settings", Context.MODE_PRIVATE);

        EditText iaName = (EditText) findViewById(R.id.ia_name);
        EditText myName = (EditText) findViewById(R.id.my_name);
        CheckBox mute = (CheckBox) findViewById(R.id.mute);
        CheckBox notify = (CheckBox) findViewById(R.id.notify_message);

        iaName.setText(pref.getString("name", "Jarvis"));
        myName.setText(pref.getString("me", ""));
        mute.setChecked(pref.getBoolean("mute", false));
        notify.setChecked(pref.getBoolean("notify", false));

        iaName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("name", s.toString());
                editor.apply();
            }
        });
        myName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("me", s.toString());
                editor.apply();
            }
        });
        mute.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("mute", isChecked);
                editor.apply();
            }
        });
        notify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("notify", isChecked);
                editor.apply();
            }
        });
    }
}
