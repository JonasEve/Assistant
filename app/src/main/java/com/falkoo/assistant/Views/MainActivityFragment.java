package com.falkoo.assistant.Views;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.falkoo.assistant.R;
import com.falkoo.assistant.Voice.Services.VoiceTask;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements SpeakerListener {

    private ImageView activated;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        view.findViewById(R.id.talk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(getContext());

                input.setInputType(InputType.TYPE_CLASS_TEXT);

                new AlertDialog.Builder(getActivity()).setMessage("Saisir une phrase").setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        VoiceTask.getInstance().process(input.getText().toString());
                    }
                }).setView(input).show();
            }
        });

        activated = (ImageView) view.findViewById(R.id.activated);

        VoiceTask.getInstance().setSpeakerListener(this);

        return view;
    }

    @Override
    public void onSpeackStatusChanged(boolean state) {
        if(state)
            activated.setImageDrawable(getActivity().getDrawable(R.drawable.rounded_green));
        else
            activated.setImageDrawable(getActivity().getDrawable(R.drawable.rounded_red));
    }
}
