package com.falkoo.assistant.Strategies.Interfaces;

import android.content.Context;

import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 28/02/2017.
 */

public interface IActionStrategy {

    String execute(Context context);
    String extractInformations(SplitableArrayList infos, Context context);

    ArrayList<String> getKeyWords();
}
