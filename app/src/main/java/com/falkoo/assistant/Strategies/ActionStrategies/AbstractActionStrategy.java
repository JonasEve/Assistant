package com.falkoo.assistant.Strategies.ActionStrategies;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.security.Key;
import java.util.ArrayList;

/**
 * Created by JEVENISSE on 06/03/2017.
 */

public abstract class AbstractActionStrategy implements IActionStrategy {

    protected boolean needToCheckKeyWord = true;
    protected String matchedKeyWord;

    public AbstractActionStrategy(String keyword){
        matchedKeyWord = keyword;
    }

    protected SplitableArrayList checkToContinue(SplitableArrayList matches, ArrayList<String> keyWord) {
        SplitableArrayList newMatches = matches.getSplitArrayList(keyWord);
        if (newMatches.size() > 0)
            return newMatches;
        else
            return null;
    }

    protected SplitableArrayList checkToContinue(SplitableArrayList matches, final String keyWord) {
        return checkToContinue(matches, new ArrayList<String>(){{add(keyWord);}});
    }

    protected ArrayList<String> getValues(SplitableArrayList splitableValues){
        ArrayList<String> values = splitableValues;

        if(needToCheckKeyWord) {
            values = checkToContinue(splitableValues, matchedKeyWord);
            needToCheckKeyWord = false;
        }

        return values;
    }
}
