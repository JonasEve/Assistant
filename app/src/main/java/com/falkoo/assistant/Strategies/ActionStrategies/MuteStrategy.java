package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.SharedPreferences;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class MuteStrategy implements IActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("tais-toi");
        add("chut");
    }};

    @Override
    public String execute(Context context) {
        SharedPreferences pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("mute", true);
        editor.apply();

        return LanguageUtils.getInstance().getLanguage().good();
    }

    @Override
    public String extractInformations(SplitableArrayList infos, Context context) {
        return null;
    }


    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
