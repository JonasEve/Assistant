package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.SharedPreferences;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class ChangeAIName extends AbstractActionStrategy {
    public String name;

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("t'appeler");
        add("ton nom est");
        add("ton nom sera");
    }};

    public ChangeAIName(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        SharedPreferences pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("name", name);
        editor.apply();

        return LanguageUtils.getInstance().getLanguage().done();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().whatsMyNewname();

        name = values.get(0);

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
