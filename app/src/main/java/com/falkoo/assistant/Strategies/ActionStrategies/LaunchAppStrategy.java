package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.Intent;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.AppUtils;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class LaunchAppStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("lancer l'application");
        add("lancer application");
        add("application");
        add("lancer");

    }};

    private String app;

    public LaunchAppStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(app);
        if (launchIntent != null) {
            context.startActivity(launchIntent);
        }

        return LanguageUtils.getInstance().getLanguage().appLauched();
    }


    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().whatApplication();

        app = AppUtils.getInstalledApp(context, values.get(0));

        if(app == null)
            return LanguageUtils.getInstance().getLanguage().invalidAppName();

        return null;
    }


    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
