package com.falkoo.assistant.Strategies.ActionStrategies;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 07/03/2017.
 */

public class ActivateDesactivateActionStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("désactiver");
        add("désactive");
        add("désactives");
        add("desactiver");
        add("desactive");
        add("desactives");
        ;
        add("activer");
        add("active");
        add("actives");
    }};

    private boolean activate;
    private String what;
    private boolean actionKnown;

    public ActivateDesactivateActionStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {

        if (TextUtils.equals(what, "wi-fi")) {
            WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(activate);
        } else if (TextUtils.equals(what, "bluetooh")) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (activate)
                bluetoothAdapter.enable();
            else
                bluetoothAdapter.disable();
        }

        return LanguageUtils.getInstance().getLanguage().function(what, activate);
    }

    @Override
    public String extractInformations(SplitableArrayList infos, Context context) {

        String value;
        if (!actionKnown) {

            if (matchedKeyWord.startsWith("dés") || matchedKeyWord.startsWith("des")) {
                activate = false;
            } else {
                activate = true;
            }

            actionKnown = true;

            SplitableArrayList values = checkToContinue(infos, matchedKeyWord);

            if (values == null)
                return LanguageUtils.getInstance().getLanguage().whatFunction(activate);

            value = values.get(0);
        } else {
            value = infos.get(0);
        }

        if (value.contains("wi-fi") || value.contains("Wi-fi") || value.contains("wifi") || value.contains("Wifi")) {
            what = "wi-fi";
        } else if (value.contains("bluetooh") || value.contains("Bluetooh")) {
            what = "bluetooh";
        }

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
