package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 01/03/2017.
 */

public class CommandStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("commande");
        add("executer");
    }};

    private String command;

    public CommandStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("command", Context.MODE_PRIVATE);
        String query = preferences.getString(command, "");
        String endPoint = preferences.getString("endpoint", "http://92.169.134.73:8080/event/{0}token=42be8b56580df434ef22dbed672dedcac61adf96");

        String url = String.format(endPoint, query);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);

        return null;
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().commandToExecute();

        SharedPreferences preferences = context.getSharedPreferences("command", Context.MODE_PRIVATE);
        String query = preferences.getString(values.get(0), null);

        if(query == null)
            return LanguageUtils.getInstance().getLanguage().commandDoesntExist();

        command = values.get(0);

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
