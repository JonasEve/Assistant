package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.telephony.SmsManager;
import android.text.TextUtils;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.ContactUtils;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 28/02/2017.
 */

public class MessageStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("envoyer un message à");
        add("envoyer un sms à");
        add("envoyer un message");
        add("envoyer un sms");
        add("envoyer message à");
        add("envoyer message");
        add("envoyer sms");
        add("envoyer sms à");
    }};

    private String who;
    private String message;

    public MessageStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(who, null, message, null, null);

        return LanguageUtils.getInstance().getLanguage().messageSent();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(who == null) {

            if (values == null)
                return LanguageUtils.getInstance().getLanguage().whoMessage();

            String contactName = values.get(0);
            who = TextUtils.isDigitsOnly(values.get(0)) ? values.get(0) : ContactUtils.getContactNumberByName(values.get(0), context, false);
            if (who == null)
                return LanguageUtils.getInstance().getLanguage().invalidContact();

            values = checkToContinue(new SplitableArrayList(values), contactName);

            if(values == null)
                return LanguageUtils.getInstance().getLanguage().whatToSend();

        } else if(message == null){
            message = values.get(0);
        }

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
