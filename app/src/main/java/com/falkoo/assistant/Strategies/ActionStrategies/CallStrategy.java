package com.falkoo.assistant.Strategies.ActionStrategies;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.ContactUtils;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 28/02/2017.
 */

public class CallStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("appeler");
        add("appel à");
        add("appel");
    }};

    private String who;

    public CallStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("tel:" + who));
        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                context.startActivity(intent);
            }
        }

        return LanguageUtils.getInstance().getLanguage().calling();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().whoCall();

        who = TextUtils.isDigitsOnly(values.get(0)) ? values.get(0) : ContactUtils.getContactNumberByName(values.get(0), context, false);
        if (who == null)
            return LanguageUtils.getInstance().getLanguage().invalidContact();

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
