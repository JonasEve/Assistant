package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class WebSearchStrategy extends AbstractActionStrategy {

    private String searchValue;

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("internet");
        add("chercher");
        add("rechercher");
    }};

    public WebSearchStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        Uri uri = Uri.parse("http://www.google.com/#q=" + searchValue);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        return LanguageUtils.getInstance().getLanguage().done();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().wantsToSearch();

        searchValue = values.get(0);

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
