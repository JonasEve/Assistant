package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.text.TextUtils;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.ContactUtils;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.MessageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by JEVENISSE on 06/03/2017.
 */

public class ReadMessageStrategy implements IActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("lire.*messages");
        add("lire.*message");
        add("lire.*messages.*");
        add("lire.*message.*");
        add("lire messages");
        add("lire message");
    }};

    private int number;
    private boolean unread;
    private String who;
    private AbstractMap.SimpleEntry<String, String> message;

    public ReadMessageStrategy(){

    }

    public ReadMessageStrategy(AbstractMap.SimpleEntry<String, String> message){
        this.message = message;
    }

    @Override
    public String execute(Context context) {

        ArrayList<AbstractMap.SimpleEntry<String, String>> messages = new ArrayList<>();

        if(message == null)
            messages = MessageUtils.getMessages(context, unread, who, number);
        else {
            messages.add(message);
            MessageUtils.setMessageRead(context, message.getKey(), message.getValue());
        }

        if(messages.size() == 0)
            return LanguageUtils.getInstance().getLanguage().noMessage();

        StringBuilder builder = new StringBuilder();

        for(AbstractMap.SimpleEntry<String, String> message : messages){
            String contactName = ContactUtils.getContactNameByNumber(message.getKey(), context);

            builder.append(contactName == null ? message.getKey() : contactName +  " : ");
            builder.append(message.getValue());
            builder.append("\n");
        }

        return builder.toString();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {

        String value = splitableValues.get(0);

        if(value.contains("non"))
            unread = true;

        if(value.contains("le")){
            number = 1;
        }

        if(value.contains("les")){
            number = Integer.MAX_VALUE;
        }

        if(value.split("de ").length > 1) {
            who = ContactUtils.getContactNumberByName(value.split("de ")[1].trim(), context, true);
            if(who == null && number == 0 && !unread)
                return LanguageUtils.getInstance().getLanguage().invalidContact();
        }

        if(number == 0 && !unread)
            return LanguageUtils.getInstance().getLanguage().dontUnderstand();

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return new ArrayList<String>(){{addAll(KEYWORDS); add("oui"); add("lire");}};
    }
}
