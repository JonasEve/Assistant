package com.falkoo.assistant.Strategies.Interfaces;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public interface ILanguageStrategy {

    String bye();
    String farewell();
    String listen();
    String dontUnderstand();
    String calling();
    String whoCall();
    String invalidContact();
    String done();
    String whatsMyNewname();
    String whatsYourNewname();
    String commandToExecute();
    String commandDoesntExist();
    String whatApplication();
    String good();
    String thanks();
    String wantsToSearch();
    String whoMessage();
    String whatToSend();
    String messageSent();
    String invalidAppName();
    String appLauched();
    String noMessage();
    String whatFunction(boolean activate);
    String function(String function, boolean activate);
}
