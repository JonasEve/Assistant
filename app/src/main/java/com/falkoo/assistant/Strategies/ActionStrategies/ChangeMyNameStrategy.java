package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;
import android.content.SharedPreferences;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.LanguageUtils;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class ChangeMyNameStrategy extends AbstractActionStrategy {

    public String name;

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("m'appeler");
        add("mon nom est");
        add("mon nom sera");
    }};

    public ChangeMyNameStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        SharedPreferences pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("me", name);
        editor.apply();

        return LanguageUtils.getInstance().getLanguage().done();
    }

    @Override
    public String extractInformations(SplitableArrayList splitableValues, Context context) {
        ArrayList<String> values = getValues(splitableValues);

        if(values == null)
            return LanguageUtils.getInstance().getLanguage().whatsYourNewname();

        name = values.get(0);

        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
