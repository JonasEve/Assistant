package com.falkoo.assistant.Strategies.ActionStrategies;

import android.content.Context;

import com.falkoo.assistant.Strategies.Interfaces.IActionStrategy;
import com.falkoo.assistant.Utils.SplitableArrayList;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 28/02/2017.
 */

public class MailStrategy extends AbstractActionStrategy {

    public static ArrayList<String> KEYWORDS = new ArrayList<String>() {{
        add("mail");
    }};

    public MailStrategy(String keyword) {
        super(keyword);
    }

    @Override
    public String execute(Context context) {
        return null;
    }

    @Override
    public String extractInformations(SplitableArrayList infos, Context context) {
        return null;
    }

    @Override
    public ArrayList<String> getKeyWords() {
        return KEYWORDS;
    }
}
