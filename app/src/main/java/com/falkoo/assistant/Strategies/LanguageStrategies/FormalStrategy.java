package com.falkoo.assistant.Strategies.LanguageStrategies;

import com.falkoo.assistant.Strategies.Interfaces.ILanguageStrategy;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class FormalStrategy implements ILanguageStrategy {
    @Override
    public String bye() {
        return "A plus tard";
    }

    @Override
    public String farewell() {
        return "Adieu";
    }

    @Override
    public String listen() {
        return "Je vous écoute";
    }

    @Override
    public String dontUnderstand() {
        return "Je n'ai pas compris";
    }

    @Override
    public String calling() {
        return "Appel en cours";
    }

    @Override
    public String whoCall() {
        return "Qui voulez-vous appeler ?";
    }

    @Override
    public String invalidContact() {
        return "Le contact n'éxiste pas, veuillez énoncer un contact valide";
    }

    @Override
    public String done() {
        return "C'est fait";
    }

    @Override
    public String whatsMyNewname() {
        return "Quel sera mon nouveau nom ?";
    }

    @Override
    public String whatsYourNewname() {
        return "Quel sera votre nouveau nom ?";
    }

    @Override
    public String commandToExecute() {
        return "Quelle commande voulez-vous executer ?";
    }

    @Override
    public String commandDoesntExist() {
        return "La commande n'éxiste pas";
    }

    @Override
    public String whatApplication() {
        return "Quelle Application ?";
    }

    @Override
    public String good(){
        return "Bien";
    }

    @Override
    public String thanks() {
        return "Merci";
    }

    @Override
    public String wantsToSearch() {
        return "Que voulez-vous rechercher ?";
    }

    @Override
    public String whoMessage() {
        return "A qui voulez-vous envoyer votre message ?";
    }

    @Override
    public String whatToSend() {
        return "Quel message voulez-vous envoyer ?";
    }

    @Override
    public String messageSent() {
        return "Le message a été bien envoyé";
    }

    @Override
    public String invalidAppName() {
        return "L'application n'éxiste pas, veuillez énoncer une application éxistante";
    }

    @Override
    public String appLauched() {
        return "Application en cours de lancement";
    }

    @Override
    public String noMessage() {
        return "Pas de messages";
    }

    @Override
    public String whatFunction(boolean activate) {
        return "Quelle fonction voulez-vous " + (activate ? "activer" : "désactiver") + "?";
    }

    @Override
    public String function(String function, boolean activate) {
        return "Le " + function + " a bien été " + (activate ? "activé" : "désactiveé") + "?";
    }
}
