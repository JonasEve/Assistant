package com.falkoo.assistant.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.falkoo.assistant.Strategies.ActionStrategies.MessageStrategy;
import com.falkoo.assistant.Strategies.ActionStrategies.ReadMessageStrategy;
import com.falkoo.assistant.Utils.ContactUtils;
import com.falkoo.assistant.Utils.MessageUtils;
import com.falkoo.assistant.Voice.Services.VoiceTask;

import java.util.AbstractMap;
import java.util.ArrayList;


/**
 * Created by JEVENISSE on 06/03/2017.
 */

public class MessageReceiver extends BroadcastReceiver {
    private SmsMessage messages;

    @Override
    public void onReceive(final Context context, Intent intent) {

        SharedPreferences pref = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        if(!pref.getBoolean("notify", false))
            return;

        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        messages =SmsMessage.createFromPdu((byte[]) pdus[0]);

        VoiceTask.getInstance().initSpeechGoogle();
        VoiceTask.getInstance().setVoiceStackState(2, new ReadMessageStrategy(new AbstractMap.SimpleEntry<>(messages.getOriginatingAddress(), messages.getMessageBody())));
        VoiceTask.getInstance().feedBack("Lire le message ?");
    }
}
