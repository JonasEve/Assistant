package com.falkoo.assistant.Utils;

import java.util.ArrayList;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class SplitableArrayList extends ArrayList<String> {

    private SplitableArrayList() {
        super();
    }

    public SplitableArrayList(ArrayList<String> init){
        super(init);
    }

    public SplitableArrayList getSplitArrayList(ArrayList<String> splitValues){

        SplitableArrayList newList = new SplitableArrayList();

        for(String value : this){
            for (String splitValue : splitValues) {
                String[] split = value.trim().split(splitValue);
                if (split.length > 1) {
                    newList.add(split[1].trim());
                }
            }
        }

        return newList;
    }

    public SplitableArrayList getSplitArrayList(final String value){

        return getSplitArrayList(new ArrayList<String>(){{add(value);}});
    }
}
