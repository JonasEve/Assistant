package com.falkoo.assistant.Utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import java.util.Iterator;
import java.util.List;

/**
 * Created by JEVENISSE on 03/03/2017.
 */

public class AppUtils {

    public static String getInstalledApp(Context context, String appName) {

        PackageManager packageManager = context.getPackageManager();
        List<ApplicationInfo> applist = packageManager.getInstalledApplications(0);

        for (ApplicationInfo pk : applist) {
            String appname = packageManager.getApplicationLabel(pk).toString();

            if(TextUtils.equals(appName, appname)){
                return pk.packageName;
            }
        }

        return null;
    }
}
