package com.falkoo.assistant.Utils;

import com.falkoo.assistant.Strategies.Interfaces.ILanguageStrategy;

/**
 * Created by JEVENISSE on 02/03/2017.
 */

public class LanguageUtils {


    private ILanguageStrategy strategy;
    private static LanguageUtils instance;

    public static LanguageUtils newInstance(ILanguageStrategy strategy) {

        if (instance == null)
            instance = new LanguageUtils(strategy);

        return instance;
    }

    public static LanguageUtils getInstance() {

        if (instance == null)
            throw new AssertionError("Context null");

        return instance;
    }

    private LanguageUtils(ILanguageStrategy strategy){
        this.strategy = strategy;
    }

    public void changeStrategy(ILanguageStrategy strategy){
        this.strategy = strategy;
    }

    public ILanguageStrategy getLanguage(){
        return strategy;
    }
}
