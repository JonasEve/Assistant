package com.falkoo.assistant.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JEVENISSE on 06/03/2017.
 */

public class MessageUtils {

    public static ArrayList<AbstractMap.SimpleEntry<String, String>> getMessages(Context context, boolean unread, String who, int number){

        if(number == 0)
            number = Integer.MAX_VALUE;

        ArrayList<AbstractMap.SimpleEntry<String, String>> messages = new ArrayList<>();

        String SORT_ORDER = "date DESC";
        Uri uriSms = Uri.parse("content://sms/inbox");
        String request = unread ? "read = 0" : null;
        if(who != null) {
            if(request != null)
                request += " AND address='" + who + "'";
            else
                request = "address='" + who + "'";
        }
        Cursor cursor = context.getContentResolver().query(uriSms, null, request, null, SORT_ORDER, null);

        cursor.moveToFirst();
        while  (cursor.moveToNext() && messages.size() < number)
        {
            String address = cursor.getString(cursor.getColumnIndex("address"));
            String body = cursor.getString(cursor.getColumnIndex("body"));

            messages.add(new AbstractMap.SimpleEntry<>(address, body));
            setMessageRead(context, address, body);
        }
        cursor.close();



        return messages;
    }

    public static void setMessageRead(Context context, String address, String body) {

        Uri uriSms = Uri.parse("content://sms/inbox");

        String selection = "address = ? AND body = ? AND read = ?";
        String[] selectionArgs = {address, body, "0"};

        ContentValues values = new ContentValues();
        values.put("read", true);

        context.getContentResolver().update(uriSms, values, selection, selectionArgs);
    }
}
